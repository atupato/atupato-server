<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Images extends Model
{
    protected $table = 'images';
    protected $fillable = [
        'model_id',
        'model_type',
        'img_url',
        'img_name'
    ];


    public function model()
    {
        return $this->morphTo();
    }
}
