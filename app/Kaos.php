<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\HasMedia\Interfaces\HasMedia;

class Kaos extends Model
{

    protected $table = 'kaos';
    protected $fillable = [
        'nama_kaos',
        'ukuran',
        'bahan',
        'sablon',
        'warna',
        'harga',
        'stok'
    ];

    public $hidden = ['media','images'];

    public function getHargaFormatAttribute()
    {
        return 'Rp '. number_format($this->harga,0,',','.');
    }

    public function images()
    {
        return $this->morphMany(Images::class,'model');
    }

}
