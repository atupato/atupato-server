<?php

namespace App\Http\Controllers;

use App\Booking;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Yajra\Datatables\Datatables;

class BookingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $booking = Booking::all();

        return view('booking.index',compact('booking'));
    }

    public function getAllData()
    {
        $booking = Booking::with('tour','user')->where('konfirmasi',1);
        return Datatables::of($booking)
            ->addColumn('bayar',function($booking){
                $bayar = $booking->adult * $booking->tour->harga;
                return 'Rp ' .number_format($bayar, 0, ',','.');
            })
            ->addColumn('action',function ($booking){
                return '';
            })
            ->make(true);
    }

    public function pending()
    {
        $booking = Booking::all();

        return view('booking.pending',compact('booking'));
    }

    public function getPendingData()
    {
        $booking = Booking::with('tour','user')->where('konfirmasi',0);
        return Datatables::of($booking)
            ->addColumn('bayar',function($booking){
                $bayar = $booking->adult * $booking->tour->harga;
                return 'Rp ' .number_format($bayar, 0, ',','.');
            })
            ->addColumn('action',function ($booking){
                return '
                <a class="btn btn-sm btn-primary" href="'.route('booking.show',['id'=>$booking->id]).'" ><i class="fa fa-list-alt"></i> Detail</a>
                <a data-confirm="Yakin ingin menghapus data ini?" data-method="delete" class="btn btn-sm btn-danger" href="'.route('booking.destroy',[$booking->id]).'" ><i class="fa fa-times"></i> Hapus</a>';
            })
            ->make(true);
    }



    public function getByUserId($id)
    {
        $result = Booking::with('tour','metodePembayaran','user')->where('user_id',$id)->get();
        for($i = 0; $i < count($result); $i++){
            $media = $result[$i]->tour->getMedia($result[$i]->tour->nama_tour);
            $arr_media = [];
            for ($j = 0; $j < count($media); $j++){
                $arr_media[] = $media[$j]->getUrl();
            }
            $result[$i]->tour['images'] = $arr_media;
        }
        return $result;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $booking = Booking::create($input);
        $result = Booking::with('tour','metodePembayaran','user')->where('id',$booking->id)->first();

        Mail::to($result->user->email)->send(new \App\Mail\OrderConfirmEmail($result));

        return $result;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $booking = Booking::findOrFail($id);
        return $booking;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
