<?php

namespace App\Http\Controllers;

use App\Gallery;
use App\Images;
use Illuminate\Http\Request;

use App\Http\Requests;

class GalleryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $gallery = Gallery::findOrFail(1);
        $media = $gallery->images;

        return view('gallery.image',compact('gallery','media'));
    }

    public function getGallery()
    {
        $result = [];

        $gallery = Gallery::findOrFail(1);
        $media = $gallery->images;

        $arr_media = [];
        for ($j = 0; $j < count($media); $j++){
            $arr_media[] = $media[$j]->img_url;
        }

        $result['gallery'] = $arr_media;
        $result['count'] = count($arr_media);

        return $result;

    }

    public function getImages()
    {
        $gallery = Gallery::findOrFail(1);
        $media = $gallery->getMedia('gallery');


        return view('clothing.image',compact('gallery','media'));
    }

    public function storeImage(Request $request, $id)
    {

        $gallery = Gallery::findOrFail($id);
        $image =  $request->file('gambar');

        $ext = $image->getClientOriginalExtension();
        $fname = str_random(16);

        $image->move(public_path('media'),$fname.'.'.$ext);

        $im = new Images();
        $im->img_url = 'media' .'/'. $fname.'.'.$ext;
        $im->img_name = $fname.'.'.$ext;

        $gallery->images()->save($im);

        return redirect()->back();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
