<?php

namespace App\Http\Controllers;

use App\Booking;
use App\BookingKonfimasi;
use Illuminate\Http\Request;

class ApiBookingController extends Controller
{
    public function  getPendingBooking($id)
    {
        $booking = Booking::with('tour','user')
            ->where('konfirmasi',0)
            ->where('user_id',$id)
            ->get();

        return $booking;
    }

    public function getBookingKonfirmasi($id)
    {
        $query = BookingKonfimasi::with('booking.tour','booking.user')
            ->whereHas('booking',function ($q) use($id) {
                  $q->where('konfirmasi',0)
                    ->where('user_id',$id);
            })->get();

        return $query;
    }

    public function getBookingFix($id)
    {
        $booking = Booking::with('tour','user')
            ->where('konfirmasi',1)
            ->where('user_id',$id)->get();

        return $booking;
    }
}
