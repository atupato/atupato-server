<?php

namespace App\Http\Controllers;

use App\KaosKonfirmasi;
use App\Order;
use Illuminate\Http\Request;

class ApiOrderController extends Controller
{
    public function  getPendingBooking($id)
    {
        $order = Order::with('kaos','user')
            ->where('konfirmasi',0)
            ->where('user_id',$id)
            ->get();

        return $order;
    }

    public function getBookingKonfirmasi($id)
    {
        $query = KaosKonfirmasi::with('order.kaos','order.user')->whereHas('order',function ($q){
            $q->where('konfirmasi',0);
        })->get();

        return $query;
    }

    public function getBookingFix($id)
    {
        $order = Order::with('kaos','user')
            ->where('konfirmasi',1)
            ->where('user_id',$id)
            ->get();

        return $order;
    }
}
