<?php

namespace App\Http\Controllers;

use App\Booking;
use App\BookingKonfimasi;
use App\Mail\KonfirmasiBerhasilEmail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Yajra\Datatables\Datatables;

class BookingKonfirmasiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $bkonfirm = BookingKonfimasi::all();
        return view('bkonfirm.index',compact('bconfirm'));
    }

    public function getTableData()
    {

        $query = BookingKonfimasi::with('booking.tour','booking.user')->whereHas('booking',function ($q){
            $q->where('konfirmasi',0);
        })->get();

        return Datatables::of($query)
            ->addColumn('bayar',function($query){
                $bayar = $query->booking->adult * $query->booking->tour->harga;
                return 'Rp ' .number_format($bayar, 0, ',','.');
            })
            ->addColumn('action',function ($query){
                return '
                 <a class="btn btn-sm btn-info" href="'.route('bkonfirm.show',['id'=>$query->id]).'" ><i class="fa fa-picture-o"></i> Detail</a>
                <a data-confirm="Yakin transaksi ini sudah benar dan anda ingin mengkonfirmasi pesanan ini??" data-method="post" class="btn btn-sm btn-success" href="'.route('bkonfirm.konfirmasi',[$query->booking->id,1]).'" ><i class="fa fa-list-alt"></i> Konfirmasi</a>
               <a data-confirm="Yakin ingin menghapus data ini?" data-method="delete" class="btn btn-sm btn-danger" href="'.route('bkonfirm.destroy',[$query->id]).'" ><i class="fa fa-times"></i> Hapus</a>';
            })
            ->make(true);
    }

    public function konfirmasi($id_booking, $konfirmasi)
    {

        $booking = Booking::findOrFail($id_booking);
        $booking->konfirmasi = $konfirmasi;
        $booking->save();

        $result = Booking::with('tour','metodePembayaran','user')->where('id',$booking->id)->first();
        Mail::to($result->user->email)->send(new KonfirmasiBerhasilEmail($result));

        return redirect()->back();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [
            'booking_id' => 'required',
            'nama_pengirim' => 'required',
            'gambar' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',

        ]);

        $input = $request->all();
        $gambar = $request->file('gambar');

        $imgExt = $gambar->getClientOriginalExtension();
        $imageName = time().'.'.str_random(6) .'.'. $imgExt;
        $gambar->move(public_path('bukti'), $imageName);

        $input['gambar'] = $imageName;

        $konfirm = BookingKonfimasi::create($input);
        $resp = BookingKonfimasi::with('booking.tour','booking.metodePembayaran','booking.user')->where('id',$konfirm->id)->first();

        return $resp;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $bkonfirm = BookingKonfimasi::findOrFail($id);

        return view('bkonfirm.view',compact('bkonfirm'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $bkonfirm = BookingKonfimasi::findOrFail($id);
        $bkonfirm->delete();

        return redirect()->back();
    }
}
