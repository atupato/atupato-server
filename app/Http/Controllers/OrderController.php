<?php

namespace App\Http\Controllers;

use App\KaosKonfirmasi;
use App\Mail\KonfirmasiBerhasilKaosEmail;
use App\Mail\OrderKaosConfirm;
use App\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Yajra\Datatables\Datatables;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('order.fix');
    }

    public function getAllData()
    {
        $order = Order::with('kaos','user')->where('konfirmasi',1);
        return Datatables::of($order)
            ->addColumn('bayar',function($order){
                $bayar = $order->kaos->harga;
                return 'Rp ' .number_format($bayar, 0, ',','.');
            })
            ->addColumn('action',function ($kaos){
                return '
                <a class="btn btn-sm btn-primary" href="'.route('booking.show',['id'=>$kaos->id]).'" ><i class="fa fa-list-alt"></i> Detail</a>
                <a data-confirm="Yakin ingin menghapus data ini?" data-method="delete" class="btn btn-sm btn-danger" href="'.route('booking.destroy',[$kaos->id]).'" ><i class="fa fa-times"></i> Hapus</a>';
            })
            ->make(true);
    }

    public function pending()
    {
        $order = Order::with('kaos','user')->where('konfirmasi',0)->get();
        return view('order.pending');
    }

    public function getPendingData()
    {
        $order = Order::with('kaos','user')->where('konfirmasi',0);
        return Datatables::of($order)
            ->addColumn('bayar',function($order){
                $bayar = $order->kaos->harga;
                return 'Rp ' .number_format($bayar, 0, ',','.');
            })
            ->addColumn('action',function ($kaos){
                return '
                <a class="btn btn-sm btn-primary" href="'.route('booking.show',['id'=>$kaos->id]).'" ><i class="fa fa-list-alt"></i> Detail</a>
                <a data-confirm="Yakin ingin menghapus data ini?" data-method="delete" class="btn btn-sm btn-danger" href="'.route('booking.destroy',[$kaos->id]).'" ><i class="fa fa-times"></i> Hapus</a>';
            })
            ->make(true);
    }

    public function konfirmasi()
    {
        return view('order.konfirmasi');
    }

    public function getKonfirmasiData()
    {

        $query = KaosKonfirmasi::with('order.kaos','order.user')->whereHas('order',function ($q){
            $q->where('konfirmasi',0);
        })->get();

        return Datatables::of($query)
            ->addColumn('bayar',function($query){
                $bayar = $query->order->kaos->harga;
                return 'Rp ' .number_format($bayar, 0, ',','.');
            })
            ->addColumn('action',function ($query){
                return '
                 <a class="btn btn-sm btn-info" href="'.route('bkonfirm.show',['id'=>$query->id]).'" ><i class="fa fa-picture-o"></i> Detail</a>
                <a data-confirm="Yakin transaksi ini sudah benar dan anda ingin mengkonfirmasi pesanan ini??" data-method="post" class="btn btn-sm btn-success" href="'.route('order.post.konfirmasi',[$query->order->id,1]).'" ><i class="fa fa-list-alt"></i> Konfirmasi</a>
               <a data-confirm="Yakin ingin menghapus data ini?" data-method="delete" class="btn btn-sm btn-danger" href="'.route('bkonfirm.destroy',[$query->id]).'" ><i class="fa fa-times"></i> Hapus</a>';
            })
            ->make(true);
    }

    public function postKonfirmasi($id_order, $konfirmasi)
    {

        $order = Order::findOrFail($id_order);
        $order->konfirmasi = $konfirmasi;
        $order->save();
        $result = Order::with('kaos','metodePembayaran','user')->where('id',$order->id)->first();
        Mail::to($result->user->email)->send(new KonfirmasiBerhasilKaosEmail($result));

        return redirect()->back();
    }


    public function getByUserId($id)
    {
        $order = Order::with('kaos','metodePembayaran')->where('user_id',$id)->get();
        return $order;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'kaos_id' => 'required',
            'user_id' => 'required',
            'metode_pembayaran_id' => 'required',
            'warna' => 'required',
            'size' => 'required'
        ]);

        $input = $request->all();
        $input['tgl_order'] = date('Y-m-d');
        $order = Order::create($input);
        $result = Order::with('kaos','metodePembayaran','user')->where('id',$order->id)->first();

        Mail::to($result->user->email)->send(new OrderKaosConfirm($result));

        return $result;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
