@extends('layouts')

@section('content')
    <div class="row">
        <!-- Page Header -->
        <div class="col-lg-12">
            <h1 class="page-header">Tour</h1>
        </div>
        <!--End Page Header -->
        <div class="row">
            <div class="col-lg-12">
                <!-- Form Elements -->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="pull-left">
                            <h4>Daftar pesanan kaos yang belum di konfirmasi client</h4>
                        </div>

                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <table class="table table-striped table-bordered table-hover" id="dataTables">
                                    <thead>
                                    <tr>
                                        <th>NO</th>
                                        <th>Nama pemesan</th>
                                        <th class="col-sm-4">Nama Kaos</th>
                                        <th>Tgl Pesanan</th>
                                        <th>Warna</th>
                                        <th>Size</th>
                                        <th>Jumlah Bayar</th>
                                        <th>Opsi</th>
                                    </tr>
                                    </thead>
                                </table>
                            </div>

                        </div>
                    </div>
                </div>
                <!-- End Form Elements -->
            </div>
        </div>
    </div>
@endsection

@push('css')
<link href="{{asset('siminta')}}/assets/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet" />
@endpush

@push('js')
<script src="{{asset('siminta')}}/assets/plugins/dataTables/jquery.dataTables.js"></script>
<script src="{{asset('siminta')}}/assets/plugins/dataTables/dataTables.bootstrap.js"></script>
<script>
    $(document).ready(function () {
        $('#dataTables').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{!! route('order.data') !!}',
            columns: [
                { data: 'id', name: 'id' },
                { data: 'user.name', name: 'user.name' },
                { data: 'kaos.nama_kaos', name: 'kaos.nama_kaos' },
                { data: 'tgl_order', name: 'tgl_order' },
                { data: 'warna', name: 'warna' },
                { data: 'size', name: 'size' },
                { data: 'bayar', name: 'bayar', "orderable": false, "searchable": false  },
                { data: 'action', name: 'action', "orderable": false, "searchable": false }
            ]
        });
    });
</script>


@endpush