@extends('layouts')

@section('content')
    <div class="row">
        <!-- Page Header -->
        <div class="col-lg-12">
            <h1 class="page-header">Konfirmasi Booking</h1>
        </div>
        <!--End Page Header -->
        <div class="row">
            <div class="col-lg-12">
                <!-- Form Elements -->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="pull-left">
                            <h4>{{ $bkonfirm->booking->tour->nama_tour }}</h4>
                        </div>

                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-12">

                                <table class="table table-hover">

                                	<tbody>
                                    <tr>
                                        <td width="200px">Nama tour</td>
                                        <td><strong>{{$bkonfirm->booking->tour->nama_tour}}</strong></td>
                                    </tr>
                                    <tr>
                                        <td>Tgl booking</td>
                                        <td>{{$bkonfirm->booking->tgl_booking}}</td>
                                    </tr>

                                    <tr>
                                        <td>Adult</td>
                                        <td>{{$bkonfirm->booking->adult}}</td>
                                    </tr>
                                    <tr>
                                        <td>Total Bayar</td>
                                        <td>{{$bkonfirm->booking->total_bayar}}</td>
                                    </tr>
                                    <tr>
                                        <td>Via</td>
                                        <td>{{$bkonfirm->booking->metodePembayaran->nama_metode}}</td>
                                    </tr>
                                    <tr>
                                        <td>Nama Pengirim</td>
                                        <td>{{$bkonfirm->nama_pengirim}}</td>
                                    </tr>

                                    <tr>
                                        <td>Bukti pembayaran</td>
                                        <td><img width="300px" height="300px" src="{{$bkonfirm->gambar}}"></td>
                                    </tr>

                                	</tbody>
                                </table>


                            </div>

                        </div>
                    </div>
                </div>
                <!-- End Form Elements -->
            </div>
        </div>
    </div>
@endsection

