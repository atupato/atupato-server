<nav class="navbar-default navbar-static-side" role="navigation">
    <!-- sidebar-collapse -->
    <div class="sidebar-collapse">
        <!-- side-menu -->
        <ul class="nav" id="side-menu">
            <li>
                <!-- user image section-->
                <div class="user-section">

                </div>
                <!--end user image section-->
            </li>

            <li class="{{ (Route::currentRouteName() == 'home') ? 'active selected' : '' }}">
                <a href="{{ route('home') }}"><i class="fa fa-dashboard fa-fw"></i>Dashboard</a>
            </li>

            <li class="{{ (Route::currentRouteName() === 'tour.index') ? 'selected' : '' }}" >
                <a href="{{route('tour.index')}}"><i class="fa fa-flask fa-fw"></i>Tour</a>
            </li>
            <li class="{{ (Route::currentRouteName() === 'clothing.index') ? 'selected' : '' }}">
                <a href="{{ route('clothing.index') }}"><i class="fa fa-table fa-fw"></i>Cloting</a>
            </li>
            <li class="{{ (Route::currentRouteName() === 'gallery.index') ? 'selected' : '' }}">
                <a href="{{ route('gallery.index') }}"><i class="fa fa-table fa-fw"></i>Gallery</a>
            </li>


            <li>
                <a href="#"><i class="fa fa-adjust"></i> Booking<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li class="{{ (Route::currentRouteName() === 'booking.pending') ? 'selected' : '' }}">
                        <a href="{{ route('booking.pending') }}"><i class="fa fa-table fa-fw"></i>Pending Booking</a>
                    </li>

                    <li class="{{ (Route::currentRouteName() === 'bkonfirm.index') ? 'selected' : '' }}">
                        <a href="{{ route('bkonfirm.index') }}"><i class="fa fa-table fa-fw"></i>Konfirmasi Booking</a>
                    </li>

                    <li class="{{ (Route::currentRouteName() === 'booking.index') ? 'selected' : '' }}">
                        <a href="{{ route('booking.index') }}"><i class="fa fa-table fa-fw"></i>Booking Fix</a>
                    </li>
                </ul>
            </li>


            <li>
                <a href="#"><i class="fa fa-adjust"></i> Order<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li class="{{ (Route::currentRouteName() === 'order.pending') ? 'selected' : '' }}">
                        <a href="{{ route('order.pending') }}"><i class="fa fa-table fa-fw"></i>Pending Order</a>
                    </li>

                    <li class="{{ (Route::currentRouteName() === 'order.konfirmasi') ? 'selected' : '' }}">
                        <a href="{{ route('order.konfirmasi') }}"><i class="fa fa-table fa-fw"></i>Konfirmasi Order</a>
                    </li>

                    <li class="{{ (Route::currentRouteName() === 'booking.index') ? 'selected' : '' }}">
                        <a href="{{ route('order.index') }}"><i class="fa fa-table fa-fw"></i>Order Fix</a>
                    </li>
                </ul>
            </li>


        </ul>
        <!-- end side-menu -->
    </div>
    <!-- end sidebar-collapse -->
</nav>