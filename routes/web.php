<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return redirect(route('home'));
});

Auth::routes();

Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout')->name('logout');

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/tour/data','TourController@getAllData')->name('tour.data');
Route::get('/tour/{id}/itenerary','TourController@getItenerary')->name('tour.itenerary');
Route::post('/tour/{id}/itenerary','TourController@storeItenerary')->name('tour.itenerary.store');
Route::get('/tour/{id}/include','TourController@getInclude')->name('tour.include');
Route::post('/tour/{id}/include','TourController@storeInclude')->name('tour.include.store');
Route::get('/tour/{id}/image','TourController@getImages')->name('tour.image');
Route::post('/tour/{id}/image','TourController@storeImage')->name('tour.image.store');

Route::resource('/tour','TourController');

Route::get('/clothing/data','ClothingController@getAllData')->name('kaos.data');
Route::get('/clothing/{id}/image','ClothingController@getImages')->name('kaos.image');
Route::post('/clothing/{id}/image','ClothingController@storeImage')->name('kaos.image.store');
Route::resource('/clothing','ClothingController');

Route::post('/gallery/{id}/image','GalleryController@storeImage')->name('gallery.image.store');
Route::resource('/gallery','GalleryController');

Route::get('booking/pending','BookingController@pending')->name('booking.pending');
Route::get('/booking/pending.json','BookingController@getPendingData')->name('booking.pending.data');
Route::get('/booking/data.json','BookingController@getAllData')->name('booking.data');
Route::resource('/booking','BookingController');


Route::post('bkonfirm/{id_booking}/{konfirmasi}/booking','BookingKonfirmasiController@konfirmasi')->name('bkonfirm.konfirmasi');
Route::get('/bkonfirm/data.json','BookingKonfirmasiController@getTableData')->name('bkonfirm.data');
Route::resource('/bkonfirm','BookingKonfirmasiController');

Route::get('/order/pending','OrderController@pending')->name('order.pending');
Route::get('/order/konfirmasi','OrderController@konfirmasi')->name('order.konfirmasi');
Route::get('/order/konfirmasi.json','OrderController@getKonfirmasiData')->name('order.konfirmasi.data');
Route::get('/order/pending.json','OrderController@getPendingData')->name('order.pending.data');
Route::get('/order/data.json','OrderController@getAllData')->name('order.data');
Route::post('/order/{id_order}/{konfirmasi}/order','OrderController@postKonfirmasi')->name('order.post.konfirmasi');
Route::resource('/order','OrderController');

Route::get('/email',function (){
    $when = Carbon\Carbon::now()->addSeconds(15);
    Mail::to("aisatriani03@gmail.com")->send(new \App\Mail\RegisterEmail());
    return 'test';
});

